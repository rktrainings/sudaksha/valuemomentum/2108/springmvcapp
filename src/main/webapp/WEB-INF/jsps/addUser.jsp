<%-- 
    Document   : addUser
    Created on : 08-Sep-2021, 8:19:32 pm
    Author     : 
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            .errorStyle{
                font-size: 14px;
                color: maroon;
                
            }
        </style>
    </head>
    <body>
        <form:form method="post" modelAttribute="currentUser" action="addUser">
            <table>
                <tr>
                    <td>Name</td>
                    <td><form:input path="name" size="30"/></td>
                    <td>
                        <form:errors path="name" cssClass="errorStyle"/>
                    </td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td>
                        <form:input path="email" size="30"/>
                    </td>
                    <td>
                        <form:errors path="email" cssClass="errorStyle"/>
                    </td>
                </tr>
                <tr>
                    <td>Remarks</td>
                    <td>
                        <form:textarea path="remarks" rows="3" cols="30"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <input type="submit" value="Add User"/>
                    </td>
                </tr>
            </table>
        </form:form>

        <form action="addUser" method="post">
            Name: <input type="text" name="name" size="30"/><br/>
            Email: <input type="text" name="email" size="30"/><br/>
            Remarks: <textarea name="remarks" cols="30" rows="4"></textarea><br/>
            <input type="submit" value="Add User"/>
        </form>
    </body>
</html>
