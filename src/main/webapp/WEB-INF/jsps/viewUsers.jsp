
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>            
        <table border="1" cellpadding="3px" cellspacing="3px">
            <tr><td>ID</td><td>Name</td><td>Email</td><td>Remarks</td></tr>

            <c:forEach items="${userList}" var="temp">
                <tr>
                    <td>${temp.userId}</td>
                    <td>${temp.name}</td>
                    <td>${temp.email}</td>
                    <td>${temp.remarks}</td>
                </tr>
            </c:forEach>
        </table>
    </body>
</html>
