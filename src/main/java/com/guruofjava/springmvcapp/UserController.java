package com.guruofjava.springmvcapp;

import java.sql.SQLException;
import java.util.List;
import javax.validation.Valid;
import javax.validation.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class UserController {

    //@Autowired
    //@Inject
    private UserService userService;

    //@Inject
    @Autowired
    private Validator validator;

    @Autowired
    private Environment env;

    public void printJDBCProperties() {
        System.out.println(env.getProperty("database.jdbc.url"));
        System.out.println(env.getProperty("database.jdbc.username"));
        System.out.println(env.getProperty("input.username.label"));
    }

    /**
     *
     * @param userService UserService object will be injected by Spring whenever
     * this controller is created.
     */
    public UserController(UserService userService) {
        this.userService = userService;
    }

    public List<User> fetchUsers() throws SQLException {
        return userService.populateUsers();
    }

    @GetMapping("/addUserForm")
    public String getAddUserForm(Model model) {
        model.addAttribute("currentUser", new User());
        return "addUser";
    }

//    @RequestMapping(value = "/addUser", method = {RequestMethod.POST})
//    public void addUser(@RequestParam("name") String name,
//            @RequestParam("mail") String email, @RequestParam("remarks") String remarks,
//            HttpServletResponse response) throws IOException {
//        userService.addUser(new User(0, name, email, remarks));
//        response.sendRedirect("users");
//    }
//    @RequestMapping(value = "/addUser", method = {RequestMethod.POST})
//    public String addUser(@RequestParam("name") String name,
//            @RequestParam("email") String email,
//            @RequestParam("remarks") String remarks, Model model) throws IOException {
//        User user = new User(0, name, email, remarks);
//        Set<ConstraintViolation<User>> errors = validator.validate(user);
//        if (errors.isEmpty()) {
//            userService.addUser(user);
//        } else {
//            System.out.println(errors);
//        }
//
//        model.addAttribute("userList", this.userService.populateUsers());
//
//        return "viewUsers";
//    }
    @RequestMapping(value = "/addUser", method = {RequestMethod.POST})
    public String addUser(Model model, @ModelAttribute("currentUser") @Valid User user, BindingResult errors) {
        if (errors.hasErrors()) {
            System.out.println(errors);
            return "addUser";
        } else {
            userService.addUser(user);
        }

        model.addAttribute("userList", this.userService.populateUsers());

        return "viewUsers";
    }

    @RequestMapping(value = "/users", method = {RequestMethod.GET})
    public String getUsers(Model model) {
        model.addAttribute("userList", this.userService.populateUsers());

        return "viewUsers";
    }

    @RequestMapping(value = "/users2", method = {RequestMethod.GET},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public @ResponseBody
    List<User> getUsers() {
        return userService.populateUsers();
    }

    @GetMapping(value = "/user/{userId}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public @ResponseBody
    User getUser(@PathVariable("userId") Integer userId) {
        return userService.getUser(userId);
    }
}
