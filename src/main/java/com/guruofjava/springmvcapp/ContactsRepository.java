/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.guruofjava.springmvcapp;

import java.util.List;
import javax.sql.DataSource;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class ContactsRepository {

    private JdbcTemplate jdbcTemplate;

    private ContactsRepository(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public Integer getContactsCount() {
        return jdbcTemplate.queryForObject("SELECT count(contact_id) FROM contacts", Integer.class);
    }

    public Contact getContact(Integer contactId) {
        return jdbcTemplate.queryForObject("SELECT * FROM contacts WHERE contact_id=?",
                new ContactRowMapper(), contactId);
    }

    public Contact getUserContact(Integer userId, Integer contactId) {
        try {
            return jdbcTemplate.queryForObject("SELECT * FROM contacts WHERE contact_id=? AND user_id=?",
                    new ContactRowMapper(), contactId, userId);
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    public void deleteContact(Integer contactId, Integer userId) {
        jdbcTemplate.update("DELETE FROM contacts WHERE user_id=? and contact_id=?",
                userId, contactId);
    }

    public void updateContact(Contact c) {
        jdbcTemplate.update("UPDATE contacts SET name=?, email=?, mobile=? WHERE contact_id=?",
                c.getName(), c.getEmail(), c.getMobile(), c.getContactId());
    }

    public void addContact(Contact contact) {
        jdbcTemplate.update("INSERT INTO contacts VALUES(?, ?, ?, ?, ?)",
                0, contact.getName(), contact.getEmail(), contact.getMobile(),
                contact.getUserId());
    }

    public List<Contact> getContactsByUser(Integer userId) {
        return jdbcTemplate.query("SELECT * FROM contacts WHERE user_id=?",
                new ContactRowMapper(), userId);
    }

    public List<Contact> getContacts() {
        return jdbcTemplate.query("SELECT * FROM contacts", new ContactRowMapper());
    }
}
