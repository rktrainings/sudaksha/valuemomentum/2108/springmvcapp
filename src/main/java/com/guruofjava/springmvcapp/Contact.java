package com.guruofjava.springmvcapp;

public class Contact {

    private Integer contactId;
    private String name;
    private String email;
    private String mobile;
    private Integer userId;

    public Contact() {
        super();
    }

    @Override
    public String toString() {
        return "Contact{" + "contactId=" + contactId + ", name=" + name + ", email=" + email + ", mobile=" + mobile + ", userId=" + userId + '}';
    }

    public Contact(Integer contactId, String name, String email, String mobile, Integer userId) {
        this.contactId = contactId;
        this.name = name;
        this.email = email;
        this.mobile = mobile;
        this.userId = userId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getContactId() {
        return contactId;
    }

    public void setContactId(Integer contactId) {
        this.contactId = contactId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

}
