package com.guruofjava.springmvcapp;

import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
//@Named
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public List<User> populateUsers() {
        try {
            return userRepository.getAllUsers();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public User getUser(Integer userId) {
        try {
            return userRepository.getUserById(userId);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public void addUser(User user) {
        try {
            userRepository.addUser(user);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
