package com.guruofjava.springmvcapp;

import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/contacts")
public class ContactsController {

    private ContactsRepository repo;

    public ContactsController(ContactsRepository repo) {
        this.repo = repo;
    }

    @RequestMapping(method = {RequestMethod.GET},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public @ResponseBody
    List<Contact> getContacts() {
        return repo.getContacts();
    }

    @RequestMapping(method = {RequestMethod.PUT}, consumes = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(HttpStatus.CREATED)
    public void updateContact(@RequestBody Contact contact) {
        Contact temp = repo.getContact(contact.getContactId());
        temp.setName(contact.getName());
        temp.setEmail(contact.getEmail());
        temp.setMobile(contact.getMobile());

        repo.updateContact(temp);
    }

    @RequestMapping(method = {RequestMethod.POST}, consumes = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(HttpStatus.CREATED)
    public void addContact(@RequestBody Contact contact) {
        System.out.println(contact);
        repo.addContact(contact);
    }

    @RequestMapping(value = "/total", produces = {MediaType.APPLICATION_JSON_VALUE})
    public Integer getContactsCount() {
        return repo.getContactsCount();
    }

    @RequestMapping(value = "/{userId}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public List<Contact> getUserContacts(@PathVariable Integer userId) {
        return repo.getContactsByUser(userId);
    }

    @RequestMapping(value = "/{userId}/{contactId}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public Contact getUserContact(@PathVariable Integer userId,
            @PathVariable Integer contactId) {
        return repo.getUserContact(userId, contactId);
    }

}
